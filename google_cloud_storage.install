<?php

/**
 * @file
 * Install, update and uninstall functions for the Google Storage module.
 */

/**
 * Implements hook_requirements().
 */
function google_cloud_storage_requirements($phase) {
  $t = get_t();
  $requirements = array();

  if ($phase != 'runtime') {
    return $requirements;
  }

  $info = libraries_load('google-api-php-client');

  if (!$info['loaded']) {
    $requirements['google_cloud_storage'] = array(
      'severity' => REQUIREMENT_ERROR,
      'title' => 'Google Cloud Storage',
      'value' => $t('Failed to load Google API Client Library'),
      'description' => $t('Please make sure the Google API Client Library for PHP is installed in the libraries directory.'),
    );
  }
  elseif (!$info['version'] || version_compare($info['version'], '2.0.0') < 0) {
    $requirements['google_cloud_storage'] = array(
      'severity' => REQUIREMENT_ERROR,
      'title' => 'Google Cloud Storage',
      'value' => $info['version'] . ' incompatible',
      'description' => $t('Please make sure the Google API Client Library for PHP installed is 2.0.0 or greater.'),
    );
  }

  if ($requirements) {
    return $requirements;
  }

  $open_ssl = extension_loaded('openssl');
  if (!$open_ssl) {
    $requirements['google_cloud_storage'] = array(
      'severity' => REQUIREMENT_ERROR,
      'title' => 'Google Cloud Storage',
      'value' => $t('Failed to load the OpenSSL PHP extension'),
      'description' => $t('Please make sure the Google API library installed is ' . google_cloud_storage_MINIMUM_VERSION . ' or greater.'),
    );
  }

  if (empty($requirements)) {
    $requirements['google_cloud_storage'] = array(
      'severity' => REQUIREMENT_OK,
      'title' => 'Google Cloud Storage',
      'value' => $info['version'],
    );
  }
  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function google_cloud_storage_uninstall() {
  variable_del('google_cloud_storage_project_id');
  variable_del('google_cloud_storage_client_id');
  variable_del('google_cloud_storage_service_account_name');
  variable_del('google_cloud_storage_key_file_path');
  variable_del('google_cloud_storage_debug');
}

/**
 * Implements hook_schema().
 */
function google_cloud_storage_schema() {
  $schema['google_cloud_storage'] = array(
    'description' => 'Stores information for files uploaded to Google Cloud Storage.',
    'fields' => array(
      'uri' => array(
        'description' => 'The URI of the file.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'binary' => TRUE,
      ),
      'filemime' => array(
        'description' => "The file's MIME type.",
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'filesize' => array(
        'description' => 'The size of the file in bytes.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The timestamp when the file is created.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('uri'),
  );
  
  return $schema;
}

/**
 * Update stream wrapper bucket naming from 1.x to 2.x.
 */
function google_cloud_storage_update_7200(&$sandbox) {
  $buckets = array();
  $fields = field_info_fields();
  foreach ($fields as $field => &$info) {
    if (isset($info['settings']['uri_scheme']) && substr($info['settings']['uri_scheme'], 0, 3) == 'gs.') {
      $buckets[] = $info['settings']['uri_scheme'];
      $info['settings']['uri_scheme'] = str_replace('-', '+', $info['settings']['uri_scheme']);

      // Update field.
      field_update_field($info);
    }
  }

  foreach ($buckets as $bucket) {
    $new_bucket_name = str_replace('-', '+', $bucket);

    db_query("UPDATE {file_managed} SET uri = REPLACE (uri, :old, :new) WHERE uri LIKE :find", array(
      ':old' => "{$bucket}://",
      ':new' => "{$new_bucket_name}://",
      ':find' => "{$bucket}://%",
    ));

    db_query("UPDATE {google_cloud_storage} SET uri = REPLACE (uri, :old, :new) WHERE uri LIKE :find", array(
      ':old' => "{$bucket}://",
      ':new' => "{$new_bucket_name}://",
      ':find' => "{$bucket}://%",
    ));
  }
}
