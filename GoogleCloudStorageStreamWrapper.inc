<?php
/**
 * @file
 *
 * Implement DrupalStreamWrapperInterface to provide gs.bucket-name:// streamwrapper.
 */
class GoogleCloudStorageStreamWrapper implements DrupalStreamWrapperInterface {
  /**
   * Instance URI (stream).
   *
   * These streams will be referenced as 'gs.bucketname://target'
   *
   * @var String
   */
  protected $uri;
  
  /**
   * The actual bucket name this file's URI is referring to.
   *
   * @var String
   */
  protected $bucket;

  /**
   * The altered bucket name.
   *
   * @var String
   */
  protected $altered_bucket_name = NULL;

  /**
   * Pointer to where we are in a directory read.
   */
  protected $directory_pointer = 0;

  /**
   * List of directories.
   */
  protected $directories;

  /**
   * List of files in a given directory.
   */
  protected $directory_list;

  /**
   * The pointer to the next read or write
   */
  protected $stream_pointer = 0;

  /**
   * A buffer for reading/wrting.
   */
  protected $stream_data = NULL;

  /**
   * The size of the stream buffer.
   */
  protected $stream_size = 0;

  /**
   * Bool HTTPS flag.
   */
  protected $https = FALSE;

  /**
   * Google Cloud Storage API domain.
   *
   */
  protected $domain = 'storage.googleapis.com';

  /**
   * Google Cloud Storage object.
   *
   */
  protected $object = NULL;

  /*
   * Google Cloud Storage client.
   */
  protected $client = NULL;
  
  /*
   * Google Cloud Storage service.
   */
  protected $google_storage = NULL;

  /*
   * Google Cloud Storage access token.
   */
  protected $access_token = NULL;

  /*
   * Google Cloud Storage JSON credential filepath.
   */
  protected $json_credential_filepath = NULL;

  /*
   * Google Cloud Storage bucket settings.
   */
  protected $bucket_settings = NULL;

  /*
   * Flag to check if uri has been altered.
   */
  protected $uri_altered = FALSE;

  /**
   * Data is not written to Google Cloud Storage in stream_write to minimize
   * requests. Instead, data is written to the $stream_data property.
   * This $write property is flagged as true, and in stream_flush, all the
   * data is written to Google Cloud Storage at once.
   *
   * @var <type> Boolean
   */
  protected $write = FALSE;

  /**
   * Object constructor.
   */
  public function __construct() {
    // Since GoogleCloudStorageStreamWrapper is always constructed with the same inputs (the
    // file URI is not part of construction), we store the constructed settings
    // statically. This is important for performance because the way Drupal's
    // APIs are used causes stream wrappers to be frequently re-constructed.
    $settings = &drupal_static('GoogleCloudStorageStreamWrapper_constructed_settings');
    if ($settings !== NULL) {
      $this->https = $settings['https'];
      $this->json_credential_filepath = $settings['json_credential_filepath'];
      $this->bucket_settings = $settings['bucket_settings'];
      $this->client = $settings['client'];
      $this->google_storage = $settings['service'];
      $this->access_token = $settings['access_token'];
      return;
    }

    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== FALSE) {
      $this->https = TRUE;
    }

    $this->json_credential_filepath = variable_get('google_cloud_storage_json_credential_file_path');
    $this->bucket_settings = variable_get('google_cloud_storage_bucket_settings');
    $this->initGoogleStorage();

    // Save all the work we just did, so that subsequent GoogleCloudStorageStreamWrapper
    // constructions don't have to repeat it.
    $settings['https'] = isset($this->https) ? $this->https : FALSE;
    $settings['json_credential_filepath'] = $this->json_credential_filepath;
    $settings['bucket_settings'] = $this->bucket_settings;
    $settings['client'] = $this->client;
    $settings['service'] = $this->google_storage;
    $settings['access_token'] = $this->access_token;
  }

  /**
   * Implements setUri().
   */
  function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Implements getUri().
   */
  function getUri() {
    return $this->uri;
  }

  /**
   * Returns the local writable target of the resource within the stream.
   * 
   * Also sets the bucket field by parsing the scheme.
   *
   * @param $uri
   *   Optional URI.
   *
   * @param $actual_bucket_name
   *   Optional If the bucket name should be the actual name.
   *
   * @return
   *   Returns a string representing a location suitable for writing of a file,
   *   or FALSE if unable to write to the file such as with read-only streams.
   */
  protected function getTarget($uri = NULL, $actual_bucket_name = TRUE) {
    if (empty($uri)) {
      $uri = $this->uri;
    }

    if (!empty($uri)) {
      list($scheme, $target) = explode('://', $uri, 2);

      if (empty($this->bucket)) {
        $parts = explode(".", $scheme);
        $scheme = $parts[0];
        unset($parts[0]);
        $bucket = implode('.', $parts);
        $this->altered_bucket_name = $bucket;
        $this->bucket = str_replace('+', '-', $bucket);
      }

      if ($actual_bucket_name) {
        // Convert bucket to actual naming for image style folders.
        $path = explode('/', $target);
  
        if ($path[0] == 'styles') {
          $path[2] = str_replace('+', '-', $path[2]);
          $target = implode('/', $path);
        }
      }

      // Remove erroneous leading or trailing, forward-slashes and backslashes.
      return trim($target, '\/');
    }

    return FALSE;
  }

  /**
   * Base implementation of getMimeType().
   */
  static function getMimeType($uri, $mapping = NULL) {
    if (!isset($mapping)) {
      // The default file map, defined in file.mimetypes.inc is quite big.
      // We only load it when necessary.
      include_once DRUPAL_ROOT . '/includes/file.mimetypes.inc';
      $mapping = file_mimetype_mapping();
    }

    $extension = '';
    $file_parts = explode('.', basename($uri));

    array_shift($file_parts);
    while ($additional_part = array_pop($file_parts)) {
      $extension = strtolower($additional_part . ($extension ? '.' . $extension : ''));
      if (isset($mapping['extensions'][$extension])) {
        return $mapping['mimetypes'][$mapping['extensions'][$extension]];
      }
    }

    return 'application/octet-stream';
  }

  /**
   * Overrides getExternalUrl().
   */
  function getExternalUrl() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing getExternalUrl (%uri)", array('%uri' => $this->uri), WATCHDOG_DEBUG);
    }
      
    //get fileinfo from the database
    $fileinfo = google_cloud_storage_get_fileinfo($this->uri);

    // if file does not exist in the database (its probably new)
    if (!$fileinfo) {
      // Image styles support
      $target = $this->getTarget($this->uri, FALSE);
      $path = explode('/', $target);

      // if its a request for an image style, the image may need to be created
      if ($path[0] == 'styles') {
        
        // Check if image exists on Google Storage
        if (!isset($this->google_storage)) {
          $this->initGoogleStorage();
        }

        try {
          $this->object = $this->google_storage->objects->get($this->bucket, $target);
        } catch (Exception $e) {
          // Trigger image style creation
          array_shift($path);
          return url('system/files/styles/' . implode('/', $path), array('absolute' => TRUE));
        }
      }
    }

    $url = '';
    $target = $this->getTarget($this->uri);
    if ($target) {
      try {
        $file_info = explode('/', $target);
        $file_name = rawurlencode(array_pop($file_info));
        $file_path = implode('/', $file_info) . '/' . $file_name;
    
        if (!empty($this->bucket_settings) && isset($this->bucket_settings[$this->bucket]['cname']) && !empty($this->bucket_settings[$this->bucket]['cname'])) {
          $bucket_base_url = $this->bucket_settings[$this->bucket]['cname'];
        }
        else {
          $secure = 'http://';
          if ($this->https) {
            $secure = 'https://';
          }
    
          $bucket_base_url = $secure . $this->bucket . '.' . $this->domain;
        }

        $url = $bucket_base_url . '/' . $target;
      }
      catch (Exception $e) {
        watchdog_exception('google_cloud_storage', $e, 'GoogleCloudStorageStreamWrapper::getExternalUrl');
      }
    }
    return $url;
  }

  /**
   * TODO chmod
   */
  function chmod($mode) {
    return TRUE;
  }

  /**
   * Implements realpath().
   */
  function realpath() {
    $target = $this->getTarget($this->uri);
    return 'gs.' . $this->altered_bucket_name . '://' . $target;
  }

  /**
   * Opens a stream, as for fopen(), file_get_contents(), file_put_contents()
   *
   * @param $uri
   *   A string containing the URI to the file to open.
   * @param $mode
   *   The file mode ("r", "wb" etc.).
   * @param $options
   *   A bit mask of STREAM_USE_PATH and STREAM_REPORT_ERRORS.
   * @param &$opened_path
   *   A string containing the path actually opened.
   *
   * @return
   *   Returns TRUE if file was opened successfully. (Always returns TRUE).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-open.php
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing stream_open (%uri)", array('%uri' => $uri), WATCHDOG_DEBUG);
    }

    $this->uri = $uri;
    $target = $this->getTarget($this->uri);

    // If this stream is being opened for writing, clear the object buffer.
    // Return true as we'll create the object on fflush call.
    if (strpbrk($mode, 'wax')) {
      $this->stream_pointer = 0;
      $this->stream_data = NULL;
      $this->write = TRUE;
      return TRUE;
    }

    if (!isset($this->google_storage)) {
      $this->initGoogleStorage();
    }

    if ($target) {
      try {
        $this->object = $this->google_storage->objects->get($this->bucket, $target);
        if ($this->object) {
          $this->write = FALSE;
          $this->stream_size = $this->object->size;
          return TRUE;
        }
      }
      catch (Exception $e) {
        watchdog_exception('google_cloud_storage', $e, 'GoogleCloudStorageStreamWrapper::stream_open');
      }
    }

    return FALSE;
  }

  /**
   * Support for fclose().
   *
   * @return
   *   TRUE if stream was successfully closed.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-close.php
   */
  public function stream_close() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing stream_close", NULL, WATCHDOG_DEBUG);
    }

    $this->clearBuffer();
    return TRUE;
  }

  /**
   * Support for flock().
   *
   * @param $operation
   *   One of the following:
   *   - LOCK_SH to acquire a shared lock (reader).
   *   - LOCK_EX to acquire an exclusive lock (writer).
   *   - LOCK_UN to release a lock (shared or exclusive).
   *   - LOCK_NB if you don't want flock() to block while locking (not
   *     supported on Windows).
   *
   * @return
   *   Always returns TRUE at the present time. (no support)
   *
   * @see http://php.net/manual/en/streamwrapper.stream-lock.php
   */
  public function stream_lock($operation) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "stream lock");
    }
    //TODO: stream_lock
    return FALSE;
  }

  /**
   * Support for fread(), file_get_contents() etc.
   *
   * @param $count
   *   Maximum number of bytes to be read.
   *
   * @return
   *   The string that was read, or FALSE in case of an error.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-read.php
   */
  public function stream_read($count) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "Reading stream (stream_read).", NULL, WATCHDOG_DEBUG);
    }

    //if the data is empty, get the data from the object
    //this stores all the data in the $stream_data property, only request once
    if (!$this->stream_data) {
      //if the object is empty, get the object from rackspace
      if (!$this->object) {
        if (!isset($this->google_storage)) {
          $this->initGoogleStorage();
        }
        
        $target = file_uri_target($this->uri);

        try {
          $this->object = $this->google_storage->objects->get($this->bucket, $target);
        } catch (Exception $e) {
          watchdog_exception('google_cloud_storage', $e, 'GoogleCloudStorageStreamWrapper::stream_read');
        }
      }

      try {
        $httpClient = $this->client->authorize();
        $response = $httpClient->request('GET', $this->object->getMediaLink());
        $this->stream_data = $response->getBody();
      } catch (Exception $e) {
        watchdog_exception('google_cloud_storage', $e, 'GoogleCloudStorageStreamWrapper::stream_read');
      }
    }
    $data = substr($this->stream_data, $this->stream_pointer, $count);
    $this->stream_pointer += $count;

    return $data;
  }

  /**
   * Support for fwrite(), file_put_contents() etc.
   *
   * @param $data
   *   The string to be written.
   *
   * @return
   *   The number of bytes written (integer).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-write.php
   */
  public function stream_write($data) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "Writing stream (stream_write).", NULL, WATCHDOG_DEBUG);
    }
    $byteswritten = strlen($data);
    $this->write = TRUE; //write when flushed
    $this->stream_data .= $data;
    $this->stream_pointer += $byteswritten;
    return $byteswritten;
  }

  /**
   * Support for feof().
   *
   * @return
   *   TRUE if end-of-file has been reached.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-eof.php
   */
  public function stream_eof() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "eof stream.", NULL, WATCHDOG_DEBUG);
    }

    if (!$this->uri) {
      return TRUE;
    }

    return ($this->stream_pointer >= $this->stream_size);
  }

  /**
   * Support for fseek().
   *
   * @param $offset
   *   The byte offset to got to.
   * @param $whence
   *   SEEK_SET, SEEK_CUR, or SEEK_END.
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-seek.php
   */
  public function stream_seek($offset, $whence) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "stream seek");
    }
    switch ($whence) {
      case SEEK_SET:
        if (strlen($this->stream_data) >= $offset && $offset >= 0) {
          $this->stream_pointer = $offset;
          return TRUE;
        }
        return FALSE;
      case SEEK_CUR:
        if ($offset >= 0) {
          $this->stream_pointer += $offset;
          return TRUE;
        }
        return FALSE;
      case SEEK_END:
        if (strlen($this->stream_data) + $offset >= 0) {
          $this->stream_pointer = $this->stream_size + $offset;
          return TRUE;
        }
        return FALSE;
      default:
        return FALSE;
    }
    return FALSE;
  }

  /**
   * Support for fflush().
   *
   * @return
   *   TRUE if data was successfully stored (or there was no data to store).
   *
   * @see http://php.net/manual/en/streamwrapper.stream-flush.php
   */
  public function stream_flush() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "flushing file, writing to Google Cloud Storage (stream_flush) %file", array('%file' => $this->uri), WATCHDOG_DEBUG);
    }

    if ($this->write) {
      if (!isset($this->google_storage)) {
        $this->initGoogleStorage();
      }
      
      $target = $this->getTarget($this->uri);

      if ($target) {
        try {
          // Because we alter bucket name, set the actual bucket name
          // location for saving file.
          $path = explode('/', $target);
          if ($path[0] == 'styles') {
            $path[2] = str_replace('+', '-', $path[2]);
            $target = implode('/', $path);
          }

          $file = new Google_Service_Storage_StorageObject();
          $file->setName($target);
          $mimetype = GoogleCloudStorageStreamWrapper::getMimeType($this->uri);
          $post_body = array(
            'mimeType' => $mimetype,
            'name' => $target,
            'data' => $this->stream_data,
            'uploadType' => 'media',
          );
          $request = $this->google_storage->objects->insert($this->bucket, $file, $post_body);
    
          if (is_object($request)) {
            // Add file info to the database
            google_cloud_storage_add_fileinfo($this->uri, $mimetype, strlen($this->stream_data));
            return TRUE;
          }
        } catch (Exception $e) {
          watchdog_exception('google_cloud_storage', $e, 'GoogleCloudStorageStreamWrapper::stream_flush');
        }
      }
    }

    $this->clearBuffer();
    return FALSE;
  }

  /**
   * Support for ftell().
   *
   * @return
   *   The current offset in bytes from the beginning of file.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-tell.php
   */
  public function stream_tell() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "Stream tell", NULL, WATCHDOG_DEBUG);
    }
    return $this->stream_pointer;
  }

  /**
   * Support for fstat().
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.stream-stat.php
   */
  public function stream_stat() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing stream_stat", NULL, WATCHDOG_DEBUG);
    }
    return $this->_stat();
  }

  /**
   * Support for unlink().
   *
   * @param $uri
   *   A string containing the uri to the resource to delete.
   *
   * @return
   *   TRUE if resource was successfully deleted.
   *
   * @see http://php.net/manual/en/streamwrapper.unlink.php
   */
  public function unlink($uri) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing unlink", NULL, WATCHDOG_DEBUG);
    }

    $target = $this->getTarget($uri);

    if (!empty($target)) {
      if (!isset($this->google_storage)) {
        $this->initGoogleStorage();
      }

      $response = $this->google_storage->objects->delete($this->bucket, $target);
      // remove file from database
      google_cloud_storage_remove_fileinfo($uri);
      return TRUE;
    }
    
    return FALSE;
  }

  /**
   * Support for rename().
   *
   * @param $from_uri,
   *   The uri to the file to rename.
   * @param $to_uri
   *   The new uri for file.
   *
   * @return
   *   TRUE if file was successfully renamed.
   *
   * @see http://php.net/manual/en/streamwrapper.rename.php
   */
  public function rename($from_uri, $to_uri) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing rename (%from, %to)", array("%from" => $from_uri, "%to" => $to_uri), WATCHDOG_DEBUG);
    }

    //TODO rename
    return TRUE;
  }

  /**
   * Support for stat().
   * This important function goes back to the Unix way of doing things.
   * In this example almost the entire stat array is irrelevant, but the
   * mode is very important. It tells PHP whether we have a file or a
   * directory and what the permissions are. All that is packed up in a
   * bitmask. This is not normal PHP fodder.
   *
   * @param $uri
   *   A string containing the URI to get information about.
   * @param $flags
   *   A bit mask of STREAM_URL_STAT_LINK and STREAM_URL_STAT_QUIET.
   *
   * @return
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   *
   * @see http://php.net/manual/en/streamwrapper.url-stat.php
   */
  public function url_stat($uri, $flags) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "Checking file status (url_stat) %file", array('%file' => $uri), WATCHDOG_DEBUG);
    }

    if (!isset($this->google_storage)) {
      $this->initGoogleStorage();
    }

    return $this->_stat($uri);
  }

  /**
   * Gets the name of the directory from a given path.
   *
   * @param $uri
   *   A URI.
   *
   * @return
   *   A string containing the directory name.
   *
   * @see drupal_dirname()
   */
  public function dirname($uri = NULL) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing dirname (%uri)", array("%uri" => $uri), WATCHDOG_DEBUG);
    }

    list($scheme, $target) = explode('://', $uri, 2);
    $target  = $this->getTarget($uri);
    $dirname = dirname($target);

    if ($dirname == '.') {
      $dirname = '';
    }

    return $scheme . '://' . $dirname;
  }

  /**
   * Support for mkdir().
   *
   * @param $uri
   *   A string containing the URI to the directory to create.
   * @param $mode
   *   Permission flags - see mkdir().
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS and STREAM_MKDIR_RECURSIVE.
   *
   * @return
   *   TRUE if directory was successfully created.
   *
   * @see http://php.net/manual/en/streamwrapper.mkdir.php
   */
  public function mkdir($uri, $mode, $options) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "mkdir");
    }

    if (!isset($this->google_storage)) {
      $this->initGoogleStorage();
    }
  
    $target  = $this->getTarget($uri);

    // Because of Google Storage "flat storage" concept,
    // we create an empty object to create a directory.
    $file = new Google_Service_Storage_StorageObject();
    $file->setName("{$target}/");
    $post_body = array(
      'mimeType' => 'application/directory',
      'name' => "{$target}/",
      'data' => '',
    );

    try {
      $this->google_storage->objects->insert($this->bucket, $file, $post_body);
      return TRUE;
    } catch (Exception $e) {
      watchdog_exception('google_cloud_storage', $e);
    }

    return FALSE;
  }

  /**
   * Support for rmdir().
   *
   * @param $uri
   *   A string containing the URI to the directory to delete.
   * @param $options
   *   A bit mask of STREAM_REPORT_ERRORS.
   *
   * @return
   *   TRUE if directory was successfully removed.
   *
   * @see http://php.net/manual/en/streamwrapper.rmdir.php
   */
  public function rmdir($uri, $options) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "rmdir");
    }

    if (!isset($this->google_storage)) {
      $this->initGoogleStorage();
    }

    $target = $this->getTarget($uri);

    try {
      $this->google_storage->objects->delete($this->bucket, "{$target}/");
      return TRUE;
    } catch (Exception $e) {
      watchdog_exception('google_cloud_storage', $e);
    }

    return FALSE;
  }

  /**
   * Support for opendir().
   *
   * @param $uri
   *   A string containing the URI to the directory to open.
   * @param $options
   *   Unknown (parameter is not documented in PHP Manual).
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-opendir.php
   */
  public function dir_opendir($uri, $options) {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing dir_opendir (%uri)", array("%uri" => $uri), WATCHDOG_DEBUG);
    }

    if (!$this->_uri_is_dir($uri)) {
      return FALSE;
    }

    if (!isset($this->google_storage)) {
      $this->initGoogleStorage();
    }

    $target = $this->getTarget($uri);

    $ls_options = array(
      'delimiter' => '/',
    );
    if (!empty($target)) {
      $ls_options['prefix'] = "{$target}/";
    }

    $directories = $this->google_storage->objects->listObjects($this->bucket, $ls_options);
    $this->directories = array();

    if (isset($directories->items)) {
      foreach ($directories->items as $item) {
        if ($item['name'] !== "{$target}/") {
          $this->directories[] = basename($item['name']);
        }
      }
    }

    // Prefixes array returns the list of directories.
    if (isset($directories->prefixes)) {
      foreach ($directories->prefixes as $item) {
        $this->directories[] = rtrim(str_replace("{$target}/", '', $item), '/\\');
      }
    }

    return TRUE;
  }

  /**
   * Support for readdir().
   *
   * @return
   *   The next filename, or FALSE if there are no more files in the directory.
   */
  public function dir_readdir() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing dir_readdir", NULL, WATCHDOG_DEBUG);
    }

    $entry = each($this->directories);
    return $entry ? $entry['value'] : FALSE;
  }

  /**
   * Support for rewinddir().
   *
   * @return
   *   TRUE on success.
   */
  public function dir_rewinddir() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing dir_rewinddir", NULL, WATCHDOG_DEBUG);
    }

    return TRUE;
  }

  /**
   * Support for closedir().
   *
   * @return
   *   TRUE on success.
   *
   * @see http://php.net/manual/en/streamwrapper.dir-closedir.php
   */
  public function dir_closedir() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "executing dir_closedir", NULL, WATCHDOG_DEBUG);
    }

    unset($this->directories);
    return TRUE;
  }

  /**
   * Initialise Google Cloud Storage connection.
   */
  public function initGoogleStorage() {
    if (variable_get('google_cloud_storage_debug')) {
      watchdog("google_cloud_storage", "initializing google_storage object", NULL, WATCHDOG_DEBUG);
    }

    if ($this->google_storage == NULL) {
      $gcs = google_cloud_storage_connect($this->json_credential_filepath);
      if (!empty($gcs)) {
        $this->client = $gcs['client'];
        $this->google_storage = $gcs['service'];
        $this->access_token = $gcs['access_token'];
      }
    }
  }

  /**
   * Flush the object buffers.
   */
  protected function clearBuffer() {
    $this->stream_data = NULL;
    $this->stream_pointer = 0;
    $this->stream_size = 0;
    $this->write = FALSE;
  }

  /**
   * Creates the fstat array.
   *
   * @param int $mode
   *   The file mode.
   * @param int $size
   *   The file size.
   * @param int $created
   *   The file created timestamp.
   *
   * @return array
   *   An array with fstat info populated or empty.
   */
  protected function createFileMode($mode, $size, $created = 0) {
    $stat = array();

    $stat[0] = $stat['dev'] = 0;
    $stat[1] = $stat['ino'] = 0;
    $stat[2] = $stat['mode'] = $mode;
    $stat[3] = $stat['nlink'] = 0;
    $stat[4] = $stat['uid'] = 0;
    $stat[5] = $stat['gid'] = 0;
    $stat[6] = $stat['rdev'] = 0;
    $stat[7] = $stat['size'] = $size;
    $stat[8] = $stat['atime'] = $created;
    $stat[9] = $stat['mtime'] = $created;
    $stat[10] = $stat['ctime'] = $created;
    $stat[11] = $stat['blksize'] = 0;
    $stat[12] = $stat['blocks'] = 0;
    $stat[4] = $stat['uid'] = 0;
    $stat[7] = $stat['size'] = $size;
    $stat[8] = $stat['atime'] = $created;
    $stat[9] = $stat['mtime'] = $created;
    $stat[10] = $stat['ctime'] = $created;

    return $stat;
  }

  /**
   * Get file status.
   *
   * @param string $uri
   *   The uri to the file.
   *
   * @return array
   *   An array with file status, or FALSE in case of an error - see fstat()
   *   for a description of this array.
   */
  protected function _stat($uri = NULL) {
    if ($this->client == NULL) {
      return FALSE;
    }
    if ($uri == NULL) {
      $uri = $this->uri;
    }
    $this->uri = $uri;

    $target = $this->getTarget($this->uri);  

    if ($target) {
      $stat = FALSE; // Default to fail.
      $mode = 0;
  
      $fileinfo = google_cloud_storage_get_fileinfo($this->uri);

      // If file does not exist in the database (its probably new)
      // Try to get it from Google Cloud Storage
      if (!$fileinfo) {  
        try {
          // Determine type: file or folder.
          $is_dir = $this->_uri_is_dir($this->uri);
          if ($is_dir) {
            // Check if folder exists.
            $directories = $this->google_storage->objects->listObjects($this->bucket, array(
              'delimiter' => '/',
              'prefix' => "{$target}/",
            ));
            if (isset($directories->items)) {
              $mode = 0040000;
              $size = 1;
              $mode |= 0777;
              $stat = $this->createFileMode($mode, $size);
              return $stat;
            }

            return FALSE;
          }

          $response = $this->google_storage->objects->get($this->bucket, $target);
          if ($response && isset($response->size) && $response->size != NULL) {
            $mode = 0100000;
            $size = $response->size;
            $mode |= 0777;
            $created = strtotime($response->timeCreated);
            $stat = $this->createFileMode($mode, $size, $created);
            return $stat;
          }
        }
        catch (Exception $e) {

        }
      }
      else {
        // fileinfo exists in the database
        $mode = 0100000;
        $filemime = $fileinfo['filemime'];
        $filesize = $fileinfo['filesize'];
        $created = $fileinfo['created'];
        $mode |= 0777;
        $stat = $this->createFileMode($mode, $filesize, $created);
        return $stat;
      }
    }

    return FALSE;
  }

  /**
   * Check if uri is a folder.
   *
   * @param string $uri
   *   The uri to the file.
   *
   * @return array
   *   TRUE if it is a folder.
   */
  protected function _uri_is_dir($uri) {
    $path_info = pathinfo($this->uri);
    return !isset($path_info['extension']) ? TRUE : FALSE;
  }
}
