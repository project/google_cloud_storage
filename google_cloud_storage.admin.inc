<?php

/**
 * @file
 * Provides Admin interface to Google Cloud Storage.
 */

/**
 * Implements hook_admin().
 */
function google_cloud_storage_admin() {
  $form['google_cloud_storage_json_credential_file'] = array(
    '#type' => 'managed_file',
    '#title' => t('JSON credential'),
    '#default_value' => variable_get('google_cloud_storage_json_credential', ''),
    '#description' => t('The generated JSON credentials that allows access to your Google Cloud Storage account. For more information, !link.', array(
      '!link' => l(t('click here'), 'https://developers.google.com/api-client-library/php/auth/service-accounts#creatinganaccount', array('attributes' => array('target' => '_blank')))
    )),
    '#required' => TRUE,
    '#upload_validators' => array(
      'file_validate_extensions' => array('json'),
    ),
    '#upload_location' => 'private://google_cloud_storage/',
    '#process' => array('google_cloud_storage_admin_public_key_file_element_process'),
  );

  if (!empty($form['google_cloud_storage_json_credential_file']['#default_value'])) {
    $buckets = google_cloud_storage_list_buckets();
    $buckets_settings = variable_get('google_cloud_storage_bucket_settings', array());

    $form['google_cloud_storage_bucket_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configure Buckets'),
      '#description' => t('Tick the checkbox to allow file system and fields setting to use the bucket.'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    );

    foreach($buckets as $id => $bucket) {
      $form['google_cloud_storage_bucket_settings'][$bucket['name']] = array(
        '#type' => 'fieldset',
        '#title' => $bucket['name'],
        '#collapsible' => TRUE,
        '#collapsed' => isset($buckets_settings[$bucket['name']]['enabled']) && $buckets_settings[$bucket['name']]['enabled'] ? FALSE : TRUE,
        '#tree' => TRUE,
      );
      $form['google_cloud_storage_bucket_settings'][$bucket['name']]['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
        '#default_value' => isset($buckets_settings[$bucket['name']]['enabled']) ? $buckets_settings[$bucket['name']]['enabled'] : FALSE,
      );
      $form['google_cloud_storage_bucket_settings'][$bucket['name']]['cname'] = array(
        '#type' => 'textfield',
        '#title' => t('CNAME for !bucket', array('!bucket' => $bucket['name'])),
        '#default_value' => isset($buckets_settings[$bucket['name']]['cname']) ? $buckets_settings[$bucket['name']]['cname'] : '',
      );
    }
  }

  $form['google_cloud_storage_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('Check the box to enable logging.'),
    '#default_value' => variable_get('google_cloud_storage_debug', FALSE),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Validate Google Cloud Storage settings.
 */
function google_cloud_storage_admin_validate($form, &$form_state) {
  $library = libraries_load('google-api-php-client');
  
  if ($library['loaded']) {
    google_cloud_storage_admin_save_key_file($form, $form_state);
    $gcs = google_cloud_storage_connect($form_state['storage']['google_cloud_storage_json_credential_file_path']);

    try {
      if (!empty($gcs)) {
        $resp = $gcs['service']->buckets->listBuckets($gcs['credentials']['project_id']);
      }
    } catch(Exception $e) {
      watchdog_exception('google_cloud_storage', $e);
      form_set_error('google_cloud_storage_json_credential_file', t('There was an error connecting the Google Cloud API. Please check your JSON credentials again.'));
    }
  }
  else {
    form_set_error('', t('Google PHP API Client Services was not loaded.'));
  }
}

/**
 * Submit hanlder for Google Cloud Storage settings.
 */
function google_cloud_storage_admin_submit($form, &$form_state) {
  variable_set('google_cloud_storage_json_credential_file', $form_state['values']['google_cloud_storage_json_credential_file']);
  variable_set('google_cloud_storage_debug', $form_state['values']['google_cloud_storage_debug']);

  $google_cloud_storage_bucket_settings = array();
  if (!empty($form_state['values']['google_cloud_storage_bucket_settings'])) {
    foreach ($form_state['values']['google_cloud_storage_bucket_settings'] as $bucket_name => $value) {
      if ($value['enabled']) {
        $google_cloud_storage_bucket_settings[$bucket_name] = $value;
      }
    }
    variable_set('google_cloud_storage_bucket_settings', $google_cloud_storage_bucket_settings);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Saves the key file.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function google_cloud_storage_admin_save_key_file(array &$form, array &$form_state) {
  // Load the file.
  $file = file_load($form_state['values']['google_cloud_storage_json_credential_file']);
  // Change status to permanent.
  $file->status = FILE_STATUS_PERMANENT;
  $file->replace = FILE_EXISTS_REPLACE;
  $uploaded = file_save($file);
  global $user;
  $account = $user;
  // Record that the module is using the file.
  file_usage_add($file, 'google_cloud_storage', 'admin', $account->uid);
  if ($uploaded) {
    $real_path = drupal_realpath($file->uri);
    variable_set('google_cloud_storage_json_credential', $file->fid);
    variable_set('google_cloud_storage_json_credential_file_path', $real_path);
    $form_state['storage']['google_cloud_storage_json_credential'] = $file->fid;
    $form_state['storage']['google_cloud_storage_json_credential_file_path'] = $real_path;
  }
}

/**
 * Remove the JSON credential file.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form_state array.
 */
function google_cloud_storage_delete_key_file($form, &$form_state) {
  $json_fid = variable_get('google_cloud_storage_json_credential');
  if ($json_fid) {
    $file = file_load($json_fid);
    if ($file) {
      file_usage_delete($file, 'google_cloud_storage');
      file_delete($file);
      variable_del('google_cloud_storage_json_credential');
      variable_del('google_cloud_storage_json_credential_file_path');
    }
    unset($file);
  }
}

/**
 * Hides the default upload button.
 *
 * @param array $element
 *   The remove element.
 * @param array $form_state
 *   The form state.
 * @param array $form
 *   The form array.
 *
 * @return mixed
 *   the form element.
 */
function google_cloud_storage_admin_public_key_file_element_process($element, &$form_state, $form) {
  $element = file_managed_file_process($element, $form_state, $form);
  $element['upload_button']['#access'] = FALSE;
  if (isset($element['remove_button']['#submit'])) {
    $element['remove_button']['#submit'][] = 'google_cloud_storage_delete_key_file';
  }
  return $element;
}

/**
 * Validate connection with Google Cloud Storage.
 */
function _google_cloud_storage_admin_test_connection_validate($form, &$form_state) {
  $info = libraries_load('google-api-php-client');
  if ($info['loaded']) {
    $response = google_cloud_storage_test_connection();
    if (empty($response)) {
      form_set_error('google_cloud_storage_json_credential_file', t('Unable to authenticate. Please check the JSON Credential, Service Account Name and Bucket Name. You can get more details in watchdog logs.'));
    }
    if (isset($response['access_token']) && $response['access_token'] && $response['bucket']) {
      $form_state['values']['#gcs_connect'] = $response;
    }
  }
  else {
    form_set_error('form', t("The Google API PHP Client library was unable to be loaded."));
  }
}

/**
 * Submit handler for Google Cloud Storage test connection.
 */
function _google_cloud_storage_admin_test_connection_submit($form, &$form_state) {
  if (isset($form_state['values']['#gcs_connect']) && isset($form_state['values']['#gcs_connect']['access_token']) && isset($form_state['values']['#gcs_connect']['bucket'])) {
    drupal_set_message(t('Connection to your Google Cloud Storage is successful!'));
  }
}